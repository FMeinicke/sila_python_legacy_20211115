# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: ObservableCommandProvider.proto

from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()


import sila2lib.framework.SiLAFramework_pb2 as SiLAFramework__pb2


DESCRIPTOR = _descriptor.FileDescriptor(
  name='ObservableCommandProvider.proto',
  package='sila2.biovt.mw.tum.de.examples.observablecommandprovider.v1',
  syntax='proto3',
  serialized_options=None,
  serialized_pb=b'\n\x1fObservableCommandProvider.proto\x12;sila2.biovt.mw.tum.de.examples.observablecommandprovider.v1\x1a\x13SiLAFramework.proto\"r\n\x18IncreaseValue_Parameters\x12+\n\x05Start\x18\x01 \x01(\x0b\x32\x1c.sila2.org.silastandard.Real\x12)\n\x03\x45nd\x18\x02 \x01(\x0b\x32\x1c.sila2.org.silastandard.Real\"\x89\x01\n\x17IncreaseValue_Responses\x12\x32\n\tInfoCalls\x18\x01 \x01(\x0b\x32\x1f.sila2.org.silastandard.Integer\x12:\n\x11IntermediateCalls\x18\x02 \x01(\x0b\x32\x1f.sila2.org.silastandard.Integer\"R\n#IncreaseValue_IntermediateResponses\x12+\n\x05Value\x18\x01 \x01(\x0b\x32\x1c.sila2.org.silastandard.Real\"\x16\n\x14Get_Value_Parameters\"B\n\x13Get_Value_Responses\x12+\n\x05Value\x18\x01 \x01(\x0b\x32\x1c.sila2.org.silastandard.Real2\xa9\x06\n\x19ObservableCommandProvider\x12\x95\x01\n\rIncreaseValue\x12U.sila2.biovt.mw.tum.de.examples.observablecommandprovider.v1.IncreaseValue_Parameters\x1a+.sila2.org.silastandard.CommandConfirmation\"\x00\x12\xb0\x01\n\x1aIncreaseValue_Intermediate\x12,.sila2.org.silastandard.CommandExecutionUUID\x1a`.sila2.biovt.mw.tum.de.examples.observablecommandprovider.v1.IncreaseValue_IntermediateResponses\"\x00\x30\x01\x12m\n\x12IncreaseValue_Info\x12,.sila2.org.silastandard.CommandExecutionUUID\x1a%.sila2.org.silastandard.ExecutionInfo\"\x00\x30\x01\x12\x9c\x01\n\x14IncreaseValue_Result\x12,.sila2.org.silastandard.CommandExecutionUUID\x1aT.sila2.biovt.mw.tum.de.examples.observablecommandprovider.v1.IncreaseValue_Responses\"\x00\x12\xb2\x01\n\tGet_Value\x12Q.sila2.biovt.mw.tum.de.examples.observablecommandprovider.v1.Get_Value_Parameters\x1aP.sila2.biovt.mw.tum.de.examples.observablecommandprovider.v1.Get_Value_Responses\"\x00\x62\x06proto3'
  ,
  dependencies=[SiLAFramework__pb2.DESCRIPTOR,])




_INCREASEVALUE_PARAMETERS = _descriptor.Descriptor(
  name='IncreaseValue_Parameters',
  full_name='sila2.biovt.mw.tum.de.examples.observablecommandprovider.v1.IncreaseValue_Parameters',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='Start', full_name='sila2.biovt.mw.tum.de.examples.observablecommandprovider.v1.IncreaseValue_Parameters.Start', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='End', full_name='sila2.biovt.mw.tum.de.examples.observablecommandprovider.v1.IncreaseValue_Parameters.End', index=1,
      number=2, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=117,
  serialized_end=231,
)


_INCREASEVALUE_RESPONSES = _descriptor.Descriptor(
  name='IncreaseValue_Responses',
  full_name='sila2.biovt.mw.tum.de.examples.observablecommandprovider.v1.IncreaseValue_Responses',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='InfoCalls', full_name='sila2.biovt.mw.tum.de.examples.observablecommandprovider.v1.IncreaseValue_Responses.InfoCalls', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='IntermediateCalls', full_name='sila2.biovt.mw.tum.de.examples.observablecommandprovider.v1.IncreaseValue_Responses.IntermediateCalls', index=1,
      number=2, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=234,
  serialized_end=371,
)


_INCREASEVALUE_INTERMEDIATERESPONSES = _descriptor.Descriptor(
  name='IncreaseValue_IntermediateResponses',
  full_name='sila2.biovt.mw.tum.de.examples.observablecommandprovider.v1.IncreaseValue_IntermediateResponses',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='Value', full_name='sila2.biovt.mw.tum.de.examples.observablecommandprovider.v1.IncreaseValue_IntermediateResponses.Value', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=373,
  serialized_end=455,
)


_GET_VALUE_PARAMETERS = _descriptor.Descriptor(
  name='Get_Value_Parameters',
  full_name='sila2.biovt.mw.tum.de.examples.observablecommandprovider.v1.Get_Value_Parameters',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=457,
  serialized_end=479,
)


_GET_VALUE_RESPONSES = _descriptor.Descriptor(
  name='Get_Value_Responses',
  full_name='sila2.biovt.mw.tum.de.examples.observablecommandprovider.v1.Get_Value_Responses',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='Value', full_name='sila2.biovt.mw.tum.de.examples.observablecommandprovider.v1.Get_Value_Responses.Value', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=481,
  serialized_end=547,
)

_INCREASEVALUE_PARAMETERS.fields_by_name['Start'].message_type = SiLAFramework__pb2._REAL
_INCREASEVALUE_PARAMETERS.fields_by_name['End'].message_type = SiLAFramework__pb2._REAL
_INCREASEVALUE_RESPONSES.fields_by_name['InfoCalls'].message_type = SiLAFramework__pb2._INTEGER
_INCREASEVALUE_RESPONSES.fields_by_name['IntermediateCalls'].message_type = SiLAFramework__pb2._INTEGER
_INCREASEVALUE_INTERMEDIATERESPONSES.fields_by_name['Value'].message_type = SiLAFramework__pb2._REAL
_GET_VALUE_RESPONSES.fields_by_name['Value'].message_type = SiLAFramework__pb2._REAL
DESCRIPTOR.message_types_by_name['IncreaseValue_Parameters'] = _INCREASEVALUE_PARAMETERS
DESCRIPTOR.message_types_by_name['IncreaseValue_Responses'] = _INCREASEVALUE_RESPONSES
DESCRIPTOR.message_types_by_name['IncreaseValue_IntermediateResponses'] = _INCREASEVALUE_INTERMEDIATERESPONSES
DESCRIPTOR.message_types_by_name['Get_Value_Parameters'] = _GET_VALUE_PARAMETERS
DESCRIPTOR.message_types_by_name['Get_Value_Responses'] = _GET_VALUE_RESPONSES
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

IncreaseValue_Parameters = _reflection.GeneratedProtocolMessageType('IncreaseValue_Parameters', (_message.Message,), {
  'DESCRIPTOR' : _INCREASEVALUE_PARAMETERS,
  '__module__' : 'ObservableCommandProvider_pb2'
  # @@protoc_insertion_point(class_scope:sila2.biovt.mw.tum.de.examples.observablecommandprovider.v1.IncreaseValue_Parameters)
  })
_sym_db.RegisterMessage(IncreaseValue_Parameters)

IncreaseValue_Responses = _reflection.GeneratedProtocolMessageType('IncreaseValue_Responses', (_message.Message,), {
  'DESCRIPTOR' : _INCREASEVALUE_RESPONSES,
  '__module__' : 'ObservableCommandProvider_pb2'
  # @@protoc_insertion_point(class_scope:sila2.biovt.mw.tum.de.examples.observablecommandprovider.v1.IncreaseValue_Responses)
  })
_sym_db.RegisterMessage(IncreaseValue_Responses)

IncreaseValue_IntermediateResponses = _reflection.GeneratedProtocolMessageType('IncreaseValue_IntermediateResponses', (_message.Message,), {
  'DESCRIPTOR' : _INCREASEVALUE_INTERMEDIATERESPONSES,
  '__module__' : 'ObservableCommandProvider_pb2'
  # @@protoc_insertion_point(class_scope:sila2.biovt.mw.tum.de.examples.observablecommandprovider.v1.IncreaseValue_IntermediateResponses)
  })
_sym_db.RegisterMessage(IncreaseValue_IntermediateResponses)

Get_Value_Parameters = _reflection.GeneratedProtocolMessageType('Get_Value_Parameters', (_message.Message,), {
  'DESCRIPTOR' : _GET_VALUE_PARAMETERS,
  '__module__' : 'ObservableCommandProvider_pb2'
  # @@protoc_insertion_point(class_scope:sila2.biovt.mw.tum.de.examples.observablecommandprovider.v1.Get_Value_Parameters)
  })
_sym_db.RegisterMessage(Get_Value_Parameters)

Get_Value_Responses = _reflection.GeneratedProtocolMessageType('Get_Value_Responses', (_message.Message,), {
  'DESCRIPTOR' : _GET_VALUE_RESPONSES,
  '__module__' : 'ObservableCommandProvider_pb2'
  # @@protoc_insertion_point(class_scope:sila2.biovt.mw.tum.de.examples.observablecommandprovider.v1.Get_Value_Responses)
  })
_sym_db.RegisterMessage(Get_Value_Responses)



_OBSERVABLECOMMANDPROVIDER = _descriptor.ServiceDescriptor(
  name='ObservableCommandProvider',
  full_name='sila2.biovt.mw.tum.de.examples.observablecommandprovider.v1.ObservableCommandProvider',
  file=DESCRIPTOR,
  index=0,
  serialized_options=None,
  serialized_start=550,
  serialized_end=1359,
  methods=[
  _descriptor.MethodDescriptor(
    name='IncreaseValue',
    full_name='sila2.biovt.mw.tum.de.examples.observablecommandprovider.v1.ObservableCommandProvider.IncreaseValue',
    index=0,
    containing_service=None,
    input_type=_INCREASEVALUE_PARAMETERS,
    output_type=SiLAFramework__pb2._COMMANDCONFIRMATION,
    serialized_options=None,
  ),
  _descriptor.MethodDescriptor(
    name='IncreaseValue_Intermediate',
    full_name='sila2.biovt.mw.tum.de.examples.observablecommandprovider.v1.ObservableCommandProvider.IncreaseValue_Intermediate',
    index=1,
    containing_service=None,
    input_type=SiLAFramework__pb2._COMMANDEXECUTIONUUID,
    output_type=_INCREASEVALUE_INTERMEDIATERESPONSES,
    serialized_options=None,
  ),
  _descriptor.MethodDescriptor(
    name='IncreaseValue_Info',
    full_name='sila2.biovt.mw.tum.de.examples.observablecommandprovider.v1.ObservableCommandProvider.IncreaseValue_Info',
    index=2,
    containing_service=None,
    input_type=SiLAFramework__pb2._COMMANDEXECUTIONUUID,
    output_type=SiLAFramework__pb2._EXECUTIONINFO,
    serialized_options=None,
  ),
  _descriptor.MethodDescriptor(
    name='IncreaseValue_Result',
    full_name='sila2.biovt.mw.tum.de.examples.observablecommandprovider.v1.ObservableCommandProvider.IncreaseValue_Result',
    index=3,
    containing_service=None,
    input_type=SiLAFramework__pb2._COMMANDEXECUTIONUUID,
    output_type=_INCREASEVALUE_RESPONSES,
    serialized_options=None,
  ),
  _descriptor.MethodDescriptor(
    name='Get_Value',
    full_name='sila2.biovt.mw.tum.de.examples.observablecommandprovider.v1.ObservableCommandProvider.Get_Value',
    index=4,
    containing_service=None,
    input_type=_GET_VALUE_PARAMETERS,
    output_type=_GET_VALUE_RESPONSES,
    serialized_options=None,
  ),
])
_sym_db.RegisterServiceDescriptor(_OBSERVABLECOMMANDPROVIDER)

DESCRIPTOR.services_by_name['ObservableCommandProvider'] = _OBSERVABLECOMMANDPROVIDER

# @@protoc_insertion_point(module_scope)

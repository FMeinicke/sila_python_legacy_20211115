from ${feature_identifier}.gRPC import ${feature_identifier}_pb2
from ${feature_identifier}.gRPC import ${feature_identifier}_pb2_grpc
# import default arguments for this feature
from ${feature_identifier}.${feature_identifier}_default_arguments import default_dict as ${feature_identifier}_default_dict
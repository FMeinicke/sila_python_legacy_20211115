SiLA2 Python3 tools
===================

silacodegenerator
------------------

The codegenerator supports the SiLA server/client developer in four
major tasks:

1. it converts the Feature Description File (FDL/XML) into a Protobuf
   .proto file
2. it compiles Protobuf .proto files
3. it generates a completely running SiLA server instance (in simulation
   mode), where only the real (or hardware) specific code needs to be
   added/subclassed to the generated prototype code.
4. it generates server/client code prototypes for testing

for further information, please refer to the README.md in the
codegenerator directory


# SiLA 2 incubators and shaker implementations
================================================


Thermo Cytomat Incubator
=========================

This is a sample implementation of a Thermo Cytomat Incubator server

Parts of it are taken from the 
csharp/features-cytomat-roche and
csharp/features-cytomat-umg branches

It is jointly developed by TU-Berlin, unitelabs and University Greifswald.

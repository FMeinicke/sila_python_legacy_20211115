#!/usr/bin/python3

import serial
import time

#file to send quick command

ser = serial.Serial("/dev/ttyUSB0", 9600, timeout=1)
i = 1
while i < 30 : 
	if len(str(i)) == 1 :
		position = "00"+str(i)
	else :
		position = "0"+str(i)
	commande = "mv:ts "+position+"\r"
	ser.write(commande.encode())
	answer = ser.readline()
	print (answer.decode())
	time.sleep(30)
	commande = "mv:st "+position+"\r"
	ser.write(commande.encode())
	answer = ser.readline()
	print (answer.decode())
	time.sleep(30)
	i =i+1

#ser.write("mv:st 001\r".encode())

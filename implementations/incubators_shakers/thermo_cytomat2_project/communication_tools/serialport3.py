#!/usr/bin/python3

import serial
import time
from answerreading import *
from serialport1 import *

"""
ser = serial.Serial(
	port="/dev/ttyUSB0",
	baudrate=9600,
	bytesyze=EIGHTBITS,
	parity=PARITY_NONE,
	stopbits=STOPBITS_ONE,
	timeout=1)
"""
ser = serial.Serial("/dev/ttyUSB0", 9600, timeout=1)


slot = "001"
tour = 0
while tour < 100 :
	Stock_out(slot)
	Stock_in(slot)
	tour+=1

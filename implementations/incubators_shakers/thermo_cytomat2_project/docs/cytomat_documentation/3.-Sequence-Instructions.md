**Sequence-Instructions**

* The [PSS](1. Terms) is controlled via [Sequence-Instructions](3. Sequence-Instructions)
*  Two main types of [Sequence-Instructions](3. Sequence-Instructions)
   *  [High-Level-Instructions](3.1 High-Level-Instructions)
   *  [Low-Level-Instructions](3.2 Low-Level-Instructions)
*  Two special types of [Sequence-Instructions](3. Sequence-Instructions)
    *  [Customer-Service-Instructions](3.3 Customer-Service-Instructions)
    *  [Heating-System- and CO2-Feed-Instructions](3.4 Heating-System- and CO2-Feed-Instructions)
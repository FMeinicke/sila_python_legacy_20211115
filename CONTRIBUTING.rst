Contributing to sila\_python
============================

First off all, thank you for considering contributing to sila\_python.
It's people like you that could bring that vision alive that in the near
future most lab devices speak one common language **SiLA**....

This is a **brief** introduction for the ones, who cannot wait to start coding for *SiLA*,
a more through introduction can be found in `contributing to sila_python <https://sila2.gitlab.io/sila_python/development/1_contributing_sila_python.html>`.

Filename Conventions
----------------------

Please follow the `PEP8 Style Guide <https://www.python.org/dev/peps/pep-0008/#id40>`_ recommendations:
  * **Python modules** should have *short, all-lowercase* names. *Underscores* can be used
    in the module name if it improves readability.
    Please choose names that tell more about what the module does, like: **serial_port_reader.py** and not **reader.py**.
  * **Python packages** should also have *short, all-lowercase names*, although the use of *underscores is discouraged*.

Coding Style
-------------

For *consistancy and better readability* of the code and not confusing the readers with too many
different styles, we would like to ask you to follow a few rules when submitting code to the **SiLA_python** repository, which are briefly:

  * please use the recommendations of `PEP8 Style Guide <https://www.python.org/dev/peps/pep-0008>`_ for code formatting, but some differences are acceptable:
  * please use a maximum of 120 characters per line
  * to differntiate between a variable name and a mothod name,  camelCased module names are also acceptable (e.g. `myExcitingNewMethod()`  )
  * please use tools like `Pylint <https://www.pylint.org/>`_ or `Pylama` to check your code readability
  * some function names might differ from the default, since all feature access functions are predefined in the SiLA standard and thus use CamelCase naming.
  * be careful with the **new python format strings** ( `f"the content of variable my_var is:  {my_var}"` ): please only use them if the refering
    variable definition is visible within **approx. 20 lines** above
  * version numbers shall follow the `Semantic Versioning Pradigm <https://semver.org/>`_
  * document your code, using `Sphinx <https://www.sphinx-doc.org>`_ inline documentation
  * please use our Sphinx *file header template* with **Overview**, **Description** of the file, **Author** and **Creation Date**

But always keep Guido's statement in mind:
*"A Foolish Consistency is the Hobgoblin of Little Minds"*

Further reading:
  `PEP8 at real python <https://realpython.com/python-pep8/>`_
  `python guide <https://docs.python-guide.org/writing/style/>`_

Submitting Issues or Asking for new features
----------------------------------------------

Please use our `Issue Tracking System <https://gitlab.com/sila2/sila_python/issues>`_  for submitting *issues* or requesting *new feature*.

Submitting your code
----------------------

If you want to contribute (even fixing small typos are very welcome) or propose a new feature implementation please feel very welcome
to send a `Merge Request <https://gitlab.com/sila2/sila_python/merge_requests>`_ .
Please mark code, which is *work in progress* with **WIP:** in the merge request title !!

git Submission
----------------

 * please use our **git-workflow** as described `git Workflow <https://sila2.gitlab.io/sila_python/development/10_git-workflow>`_
 * start a new feature branch with `git flow feature start [my_new_feature] `
 * add short, but clear **commit statements**
 * when you generate a new branch and push it to gitlab, please use the --set-upstream (or -u) parameter the first
   time you push to GitLab, like `git push --set-upstream origin feature-my-new-feature`
 * we want an ultrafast user experience: newcomers should be ready to develop in less than 10min, therefore
   the repository shall be kept as small and lean as possible - have always a
   developer on a raspberry pi in mind with a slow internet connection: transmitting even 20 MB could be a pain.
 * avoid larger binaries, like .jpg/.png /.jar (>200kb) files they blow up
   the repository and slow down the download speed.
   In case you want to include binary files, please use the `git lfs extension <https://docs.gitlab.com/ee/administration/lfs/manage_large_binaries_with_git_lfs.html>`_
 * add version numbers and tags shall follow the `Semantic Versioning Pradigm <https://semver.org/>`_

Acknowledgements
-----------------

We acknowledge and **thank** the following contributors to this repository:

.. include:: CONTRIBUTORS.rst

Furthermore we would like to thank the large open source communities
without this project would be impossible:

-  python community
-  gRPC community
-  zeroConfig developers

**A BIG THANX TO ALL !!**

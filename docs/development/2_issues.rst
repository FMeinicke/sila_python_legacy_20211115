Submitting Issues
==================

Please use our gitLab Issue page to submitt issues: _sila-python-gitlab-issues.

If you write an issue, please state the issue clearly - also to which part of sila_python it refers.
Please think already of solution / suggestion or search the web for similar solutions.
This makes it much easier for the developers to quickly come up with a fix.

sila_python installation on Microsoft Windows 10
================================================

Please install python 3.7 (choose 64bit if your machine supports it).
(the grcpio library for python 3.8 is not available as precompiled binary - status: 19/11 )

For basic usage / testing of the examples , just type

.. code-block:: console

   pip install sila2lib

If you prefer to use a virtual environment, we prepared an installer script:

.. code-block:: console

   python3 sila2installer.py # runs only with python >= 3.6


Troubleshooting
----------------

In case the installation routine throws the following error:

.. code-block:: python


    raise distutils.errors.DistutilsPlatformError(
    distutils.errors.DistutilsPlatformError: Microsoft Visual C++ 14.0 is required. Get it with "Build Tools for Visual Studio": https://visualstudio.microsoft.com/downloads/


Please try to install "*Build Tools for Visual Studio*": https://visualstudio.microsoft.com/downloads/
The *build tools* are quite hidden on the Microsoft page - on the very bootom, please go to:
 "Tools für Visual Studio 2019 -> Build Tools for Visual Studio" (it is a rather small file of ca. 1.3 MB)

 The grcpio library for python 3.8 is not available as precompiled binary - status: 19/11

 **Consider to use python 3.7 until this is fixed.**

![sila-python](docs/images/sila-python-logo.png)

[SiLA](https://sila-standard.com) Python3 Repository
=====================================================

[![docs](https://img.shields.io/badge/docs-passing-brightgreen.svg)](https://sila2.gitlab.io/sila_python)
[![pypi](https://img.shields.io/pypi/v/sila2lib.svg)](https://pypi.python.org/pypi/sila2lib/)
[![python](https://img.shields.io/pypi/pyversions/hypercorn.svg)](https://pypi.python.org/pypi/sila2lib/)
[![license](https://img.shields.io/badge/license-MIT-blue.svg)](%3Chttps://gitlab.com/sila2/sila_python/blob/master/LICENSE)

**Welcome to SiLA2 and Python 3 !**

*SiLA_python* is a python implementation of the universal and royalty free **[SiLA 2](https://sila-standard.org)** laboratory automation standard.
It provides convenient libraries, a codegenerator and a collection of examples and implementations to support a fast integration of SiLA 2 into your own lab automation projects and to illustrate how SiLA 2 could be implemented.  

This is currently the fastest route to a SiLA2 experience. With the
SiLA2 python installer - *sila2install.py* (s.
[Quickstart](#quickstart)) - you will be able to run a first SiLA2
HelloWorld Service within *10 minutes* !

Our new **silacodegenerator** makes it possible to do a very fast prototyping of server/clients within hours or days.
Please read further in the [silacodegenerator README.md ](https://gitlab.com/sila2/sila_python/tree/master/sila_tools/sila2codegenerator/README.md) how to use it.  


*Please note that **Python &gt;= 3.6 or 3.7** is mandatory to run the code in this repository* (grpcio binary support for python 3.8 under MSWindows is still missing).

Status
------
This repository tries to reflect a wide range of the **1.0 Release of SiLA 2 specification**.

For more general information about the standard, we refer to the
[sila\_base](https://gitlab.com/SiLA2/sila_base) repository.

`!! encryption is supported autoamtically, if you (currently) place a certificate and key file in the same location as the server/client files`

Maintainer
----------

This repository is currently maintained by [University of
Greifswald](https://lara.uni-greifswald.de/)

Please contact:

-   Mark Doerr (<mark.doerr@uni-greifswald.de>)


Components
----------

### [sila\_library](https://gitlab.com/sila2/sila_python/tree/master/sila_library)

A python library to aid the development of SiLA Servers. The library
contains, e.g. the SiLAserver/client base classes, zeroconfig/bonjour server detection,
SiLA feature definition (FDL) parser, SiLA Service and the Error Handling library.

### [sila\_com\_library](https://gitlab.com/sila2/sila_python/tree/master/sila_com_library)

A python communication library for hardware communication (like serial/RS232, CAN, SPI, GPIO).

### [examples](https://gitlab.com/sila2/sila_python/tree/master/sila_examples)

A collection of example applications that demonstrate important concepts
of SiLA2. A good starting point to learn python_sila is:

-   [Hello\_SiLA2](https://gitlab.com/sila2/sila_python/tree/master/examples/HelloSiLA2) - a minimalistic "Hello SiLA 2" Service.

### [sila\_tools](https://gitlab.com/sila2/sila_python/tree/master/sila_tools)

Tools to support the rapid development of SiLA2 Servers/Services. It is
highly recommended to start each individual project with the powerful
codegenerator, since it already provides you with a functional
framework:

-   [codegenerator](https://gitlab.com/sila2/sila_python/tree/master/sila_tools/sila2codegenerator) -
    this is a powerful tool to generate a complete SiLA2 set of
    server/client, solely based on Python3, out of a few
    configuration files.

### [implementations](https://gitlab.com/sila2/sila_python/tree/master/implementations)

This folder provides Proof-of-Concept (POC) implementations for typical lab
devices and other common tasks - it is merely for demonstration purposes.

### [raspberry-pi](https://gitlab.com/sila2/sila_python/tree/master/raspberry_pi)

All information you need to run *SiLA2 server/clients* on a **Raspberry Pi**

Installation
-------------

The simplest way, to install SiLA\_python is running the
*sila2installer.py* script\* - mind: it requires Python 3.6.0 or higher.

``` {.sourceCode .console}
$ sudo apt install python3-pip  # this might be necessary under Linux  
$ pip3 install python3-venv
$ sila2installer.py
```

For further installation methods, please see:
[Installation](https://gitlab.com/sila2/sila_python/tree/master/docs/installation/0_installation.rst)


Quickstart Tutorials
--------------------

Get SiLA2 running in less than 5-10 minutes (depending on your internet connection)
with our [Quickstart Tutorial](https://sila2.gitlab.io/sila_python/tutorials/1_quickstart.html)

For ultra-fast code generation, please look into our sila2codegenerator quickstart tutorial:
  [sila2codegenerator tutorial](https://gitlab.com/sila2/sila_python/tree/master/sila_tools/sila2codegenerator)

For fast installation of SiLA 2 Server on a Raspberry Pi 3/B+  
  [Raspberry Pi Installation](https://gitlab.com/sila2/sila_python/tree/master/raspberry_pi)


### [sila_python YouTube Channel](https://www.youtube.com/channel/UCKyGU-Qv5B3yJWQxLnUOyuA)

Please visit our [sila_python YouTube Channel](https://www.youtube.com/channel/UCKyGU-Qv5B3yJWQxLnUOyuA) for introductions and tutorials.


[Documentation](https://sila2.gitlab.io/sila_python)
--------------------------------------------------------
The documentation can be found here: [Documentation](https://sila2.gitlab.io/sila_python)

[Contributing](https://sila2.gitlab.io/sila_python/development/1_contributing_sila_python.html)
--------------------------------------------------------------------------------------------------------------------

**Please help us to make it a great starting point for SiLA2 !** SiLA\_python is developed on
[GitLab](https://gitlab.com/sila2/sila_python) by developers and volunteers like you !
If you come across an issue, or have a feature request please open an
**[issue](https://gitlab.com/sila2/sila_python/issues)**. If you want to
contribute (even fixing small typos are very welcome) or propose a new feature implementation please feel very welcome
by sending a [merge request](https://gitlab.com/sila2/sila_python/merge_requests). **We need all hands and all your help - thanks !**

For all the details of contributing, please look into: [Contributing](https://sila2.gitlab.io/sila_python/development/1_contributing_sila_python.html).

Help
----

The SiLA\_python [documentation](https://sila2.gitlab.io/sila_python)
is the best place to start, after that - and if you cannot find, what
you are looking for -contact our Slack-channel: [Slack \#sila\_python](https://join.slack.com/t/sila-standard/shared_invite/enQtNDI0ODcxMDg5NzkzLTc4YjdkNzgxYjM5NDIyMzAyNTJjMjE1ZWI5MzY0M2Y2NmY3ZGQ2NTI3YzJiMmIzNTFmZmJkMWI3ZTMyMTk5NGY)

If you still can't find an answer please [open an
issue](https://gitlab.com/sila2/sila_python/issues).

The official SiLA-Standard specifications can be found at [https://sila-standard.org](https://sila-standard.org).

Testing
-------

The best way to test SiLA\_python is with
[Tox](https://tox.readthedocs.io), or nose2:

``` {.sourceCode .console}
$ pip install tox
$ tox
```

this will check the code style and run the tests.

License
=======

This code is licensed under the [MIT
License](https://en.wikipedia.org/wiki/MIT_License).

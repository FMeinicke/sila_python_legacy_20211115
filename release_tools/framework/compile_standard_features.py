#!/usr/bin/env python3
"""
________________________________________________________________________

:PROJECT: SiLA2_python

*Compile standard features*

:details: This script converts and compiles the ``.sila.xml`` files in the sila_library
          (``sila_library/sila2lib/framework/feature_definitions/.../*.sila.xml``) to the corresponding python
          ``_pb2.py`` and ``_pb2_grpc.py`` files. It should be executed after the ``.sila.xml`` files
          have been updated from the `sila\\_base <https://gitlab.com/SiLA2/sila_base>`_ repository.

:file:    compile_standard_feature.py
:authors: Timm Severin

:date: (creation)          2019-08-26
:date: (last modification) 2019-08-26
________________________________________________________________________

"""

# pylint: disable=invalid-name

import os

from typing import List

import logging

from sila2lib.fdl_parser.fdl_parser import FDLParser
from sila2lib.proto_builder.proto_builder import ProtoBuilder
from sila2lib.proto_builder.proto_compiler import compile_proto

# logging details
logging.basicConfig(format='%(levelname)s | %(module)s.%(funcName)s: %(message)s', level=logging.INFO)

# path to the directory of the SiLA standard definitions
input_path = os.path.join('..', '..', 'sila_library', 'sila2lib', 'framework', 'feature_definitions')

# target directory of generated stubs ( _pb2.py, _pb2_grpc.py files) and proto files
output_dir_proto = os.path.join('..', '..', 'sila_library', 'sila2lib', 'framework', 'protobuf')
output_dir_stubs = os.path.join('..', '..', 'sila_library', 'sila2lib', 'framework', 'std_features')

# use subdirectories for the generated files or not (not using subdirectories can lead in definitions overwriting each
# other if two definitions with the same name exist in different subdirectories)
use_proto_subdirectories = True
use_stubs_subdirectories = False

logging.info('Reading all SiLA feature definitions from `{input_path}`'.format(input_path=input_path))
logging.info('Writing .proto output to `{output_dir}`'.format(output_dir=output_dir_proto))
logging.info('Writing compiled files output to `{output_dir}`'.format(output_dir=output_dir_stubs))


# noinspection PyShadowingNames
def _find_feature_definitions(input_directory: str) -> List[str]:
    """
    Function that recursively searches an input directory to find all SiLA feature definitions.

    :param input_directory: The input directory where to search.

    :return: List of all files with path relative to the input directory.
    """

    # prepare an empty output list
    fdl_files: List[str] = []

    for path_element in os.listdir(input_directory):
        current_path = os.path.join(input_directory, path_element)

        if os.path.isdir(current_path):
            # recursively construct search all subdirectories
            fdl_files.extend([
                os.path.join(path_element, current_fdl_file)
                for current_fdl_file
                in _find_feature_definitions(input_directory=current_path)
                ])
        else:
            if path_element[-9:].lower() == '.sila.xml':
                fdl_files.append(path_element)

    return fdl_files


input_files = _find_feature_definitions(input_directory=input_path)

logging.info('Found {count} feature definitions: '.format(count=len(input_files)))
for file in input_files:
    logging.info('    * {filename}'.format(filename=file))

for file in input_files:
    # Convert FDL files to .proto
    #   Create paths
    subdirectory = os.path.dirname(file)
    filename = os.path.basename(file)
    target_dir = output_dir_proto if not use_proto_subdirectories else os.path.join(output_dir_proto, subdirectory)
    logging.info(
        'Converting "{filename}" from directory "{source_directory}" to .proto in "{target_directory}"'.format(
            filename=filename,
            source_directory=subdirectory,
            target_directory=target_dir
        )
    )

    #   Make sure the output directory exists
    os.makedirs(target_dir, exist_ok=True)
    #   Actual conversion
    fdl_parser = FDLParser(fdl_filename=os.path.join(input_path, subdirectory, filename))
    proto_builder = ProtoBuilder(fdl_parser=fdl_parser)
    proto_file = proto_builder.write_proto(proto_dir=target_dir)
    logging.info('Written result to {proto_file}'.format(proto_file=proto_file))

    # Compile the new file to python
    #   Create paths
    proto_dir = os.path.dirname(proto_file)
    proto_file = os.path.basename(proto_file)
    target_dir = output_dir_stubs if not use_stubs_subdirectories else os.path.join(output_dir_stubs, subdirectory)
    logging.info(
        'Compiling proto file "{proto_file}" to python into directory "{target_directory}"'.format(
            proto_file=proto_file,
            target_directory=target_dir
        )
    )

    #   Make sure the output directory exists
    os.makedirs(target_dir, exist_ok=True)
    #   Actual conversion
    if compile_proto(proto_file=proto_file, source_dir=proto_dir, target_dir=target_dir,
                     auto_include_library=True):
        logging.info('File compile successfully.')
    else:
        logging.warning('Error while compiling proto file "{proto_file}"'.format(
            proto_file=os.path.join(proto_dir, proto_file)
        ))

    (pb2_files, _) = os.path.splitext(os.path.basename(proto_file))
    pb2_grpc_file = pb2_files + '_pb2_grpc.py'
    pb2_grpc_file = os.path.join(target_dir, pb2_grpc_file)
    with open(pb2_grpc_file, 'r', encoding='utf-8') as file_in:
        logging.debug('Correcting {file}'.format(file=pb2_grpc_file))
        logging.debug('\t' 'Correcting for import of _pb2')
        replaced_text = file_in.read()
        replaced_text = replaced_text.replace(
            'import {pb2}_pb2 as {pb2}__pb2'.format(pb2=pb2_files),
            'from . import {pb2}_pb2 as {pb2}__pb2'.format(pb2=pb2_files))
    with open(pb2_grpc_file, 'w', encoding='utf-8') as file_out:
        file_out.write(replaced_text)
